# evoke 7.1 IHM February 2021

evoke 7.0 has been abandoned, as development got stuck.

SO evoke 7.1 was re-ported from evoke 6.1



## significant changes to evoke 7.1


### configuration change

Page.config.attribution option replaced by Page.config.show_who
- options True or False.
- affects listings only. Page headers now always attribute author.


### evoke.Page enhancements

evoke.Page.tagline() replaced by Page_byline.evo

some code factored out into mix-in classes

- search.py
- tags.py
- ratings.py

rating-filter code added

### changes to file kinds

For all file kinds (file, image):

- deletion of the page no longer deletes its associated file
- file deletion is achieved via pare_data utility (called from browser url e.g. http://mydomain/pare_data )
- multiple file kinds now can now link to one associated file (they simply have the same code field)


#### tagging and tag search added:

- Tag.py klass added for tags

- tagging is done via a "tags" tab

- tags are selected from a tag cloud based on tags used by the parent page and all its descendents

- new tags are easily added to the tag cloud

- tag search is available via the "search" tab (see search enhancements below)


#### search enhancements:

- text search has been replaced by an all new version

- tag search added, allowing AND and NOT tag combinations

- search (both text and tags) is now accessed via a "search" tab

- both text and tag search are done within the scope of the parent page and all its descendants. Note that, for page 1 and its immediate children, this gives a global search. This approach may not seem intuitive, but is consistent and works well at the leaf level (non-parent pages)

- search results now page (via Page_listing_content.evo), so search result sets are now effectively unlimited

- search is no longer full-text (as that was disappointing and limiting in practice), and is now a simple text match (case insensitive)

- for text search: page uids, names, and texts are searched

- order-by options have been added to both text and tag searches


#### current tab (Page_options.evo) is now identified via req.tab rather than req.page

- req.page remains in use, as before, for the method to call when paging a listing

- req.tab is simply set to the tab name (except for "view" tab which is compared to self.kind). This is much more intuitive in use than was the previous setup.


#### unified display code for listing of page summaries

- all listings of page summaries now use the same display code: Page_listing_content.evo . This includes:
  - listings (as before - paged results, 50 items at a time)
  - child page displays on page view, page edit, etc (formerly had separate code, essentially the same)
  - search and tag-search results (formerly had separate and different code which didn't page at all) 
 

### evoke.lib enhancements

lib.page(req,pagesize) now sets req.SQLlimit (for use as SQL limit parameter)

- otherwise lib.page works as before, setting req.pagenext and req.pagesize, and returning SQLlimit


#### lib.DATE() enhancements

- constructor now accepts isoformat time (in addition to formats already accepted)

- lib.DATE().nice() parameter "long" renamed "time" as more meaningful (still defaults to true)


#### lib.http_date(future=0) added as an elegant replacement for httpDate()

- note: these have different parameters - not directly substitutable

- httpDate has been moved to lib.deprecated.py (but for now remains in the lib interface)





## significant changes applied to version 6.1, and thereby in evoke 7.1 also

### evoke data optimisation (patch by CJH 16/12/2018 per evoke 7.0 master)
20/11/2020

data.get - put the dict for an object directly into ob.__dict__  don't cast as Schema class.

data.__getattribute__ - if a value hasn't been cast into Schema, do so before returning.

Page.get - cache override kind classes rather than recreating each tie.


### evoke ratings upgrade:
31/10/2020

was:

2 = love
1 = like
0 = ? (unsure)
-1 = disabled 2
-2 = disabled 1
-3 = disabled 0
-4 = x (don't want)

now:

3 = great
2 = good
1 = ok
0 = ? (unsure)
-1 = x (don't want)
-2 = disabled 3
-3 = disabled 2
-4 = disabled 1
-5 = disabled 0

Implemented and working.

No explicit conversion required, as existing data will convert fairly gracefully: However enabled "heart" items now become diamonds (i.e. one step down) and there will be no heart items (so the heart symbol has been "promoted") until you create some by uprating pages.

Disabled items will be uprated one step - this is a side effect of the half-baked rating correlations used. If this is a problem, do a manual fix to decrement all page ratings lower than -1 by 1.
