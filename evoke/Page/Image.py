""" Images -  grace.Page kind (adapted for evoke 7)

mix-in class for Page.py

- allows child pages of kind=="image" to be added which link to viewable image files
- depends on File class

Notes:
- images are renamed and stored in a data/<appname>/ subfolder as <uid>.<ext> where:
    - <uid> is the uid of the image page created when that image file was added
    - <ext> is the original file extension
- an image page (code field) can also refer to an existing image file, allowing multiple image pages to share one image file
- when an image page is removed (deleted), the corresponding file is left in place in its data/<appname> subfolder
    - this avoids complications with shared image files
- use the pare_data utility routine (in File class) to safely delete all files which are no longer referenced by any page


Ian Howie Mackenzie (2007, May 2019, March 2020, March 2022)

(O/S - look at using webp rather than jpg for thumbnail format)
"""

from io import BytesIO
from os.path import lexists
import imghdr

from PIL import Image as pim

from evoke.lib import *
from evoke.render import html


class ImageError(Exception):
    """Base class for exceptions in this module."""
    pass

class Image(object):
    ""
    filekinds = ["file","image"] # extends the File class filekinds

    def edit_image(self, req):
        "image edit handler"
        req.page = 'edit'
        if "name" in req:  #we have an update
            # store changes
            self.update(req)
            self.flush()
        return self.edit(req)


#    edit_image.permit = 'edit page'

    @classmethod
    def convert_image(self, filedata):
        "assumes filedata is BMP or some other compatible format - converts to JPG"
        bmp = BytesIO(filedata)
        bmp.reset()
        img = pim.open(bmp)
        f = BytesIO()
        img.convert('RGB').save(f, 'JPEG')
        f.reset()
        return f.read()

# thumbnails #########################################

    def thumb_url(self):
        """url for a jpeg thumbnail of the image 
       - makes the thumbnail on the fly, if necessary
       - currently makes thumbnail copies even for small files, regardless
       - thumbnail size is determined by self.Config.thumb_size
    """
        if not lexists(self.thumb_loc()):
            try:
                self.make_thumb()
            except ImageError:
                raise
                print("**** CANNOT THUMBNAIL:", self.file_loc())
                return self.file_url()
        return f"/site/data/{self.thumb_folder()}/{self.thumb_name()}"

    def thumb_loc(self):
        "location of jpeg thumbnail of the image - the thumbnail may not exist"
        return f"{self.Config.site_filepath}data/{self.thumb_folder()}/{self.thumb_name()}"

    def thumb_folder(self):
        "folder (within data folder) for thumbnails - this can be overridden by apps"
        return self.file_folder() + '/thumbs'

    def thumb_name(self):
        "filename for a jpeg thumbnail of the image - the thumbnail may not exist"
        #    return "%s.%s" % (self.uid,self.get_extension())
        return "%s.jpg" % self.uid

#  def get_extension(self):
#    "JPEG, GIF, or PNG"
#    return self.code.split(".")[-1].lower()

    def make_thumb(self, width=0):
        "creates and stores a jpeg thumbnail of the image"
        #print("making thumb for: ",self.uid)
        im = open(self.file_loc(), 'rb')
        x = pim.open(im)
        #    print 'x.size:',x.size[1], x.size[0]
        size = width or safeint(self.Config.thumb_size) or 200
        #print(">>>>>>>>>>>>>>>>>>>>> creating thumb:",size)
        #print(">>>>>>>>>>>>>>>>>>>>> thumb_size:",self.Config.thumb_size)
        #print(">>>>>>>>>>>>>>>>>>",size, x.size[1], x.size[0])
        try:
            x.thumbnail((size, size * x.size[1] / x.size[0]), pim.ANTIALIAS)
        except Exception as e:
            print("ERROR MAKING THUMBNAIL for %s:%s" % (self.uid, e))
        f = BytesIO()
        #    extension=self.get_extension().upper()
        #    x.convert(extension!='GIF' and 'RGB' or None).save(f,extension) #this creates thumbs of correct type but the non-jpg ones can be much bigger than the originals!
        x.convert('RGB').save(f, 'JPEG')
        f.seek(0)  # back to the start..
        self.save_file(f.read(), self.thumb_folder(), self.thumb_name())

    def test_thumb(self, req):
        self.get_thumb()
        req.message = 'thumbnail saved'
        return self.view(req)

#### add / remove an image #######

    def add_image(self, req):
        ""
        image = self.image_saved(req)
        if hasattr(self, 'images'):
            del self.images  # clear cache
        if image:
            return self.redirect(req, "")
        return self.image_add(req)

#    add_image.permit = 'edit page'

    def image_saved(self, req):
        """
        if req is valid, stores new image and returns image object, else returns 0. 
        - REQUIRES req.filedata and req.filename
        - Folder can optionally be specified in req.folder
        """
        if 'filedata' in req:
            filedata = req.filedata
            extension = (imghdr.what('', filedata)
                         or req.filename.split(".")[-1].lower()).replace(
                             'jpeg', 'jpg')
            if not filedata:
                req.error = "please provide an image file"
            elif extension == 'bmp':
                filedata = self.convert(filedata)
                extension = 'jpg'
            elif extension not in ('gif', 'png', 'jpg', 'jpeg', 'webp'):
                req.error = "only JPEG, GIF, PNG, BMP, and WEBP are supported - please select another image"
            if not req.error:  # check that size is not too big
                if len(filedata) > 1 << 20:
                    req.error = "image size exceeds 1 megabyte - please use a smaller image"
            if not req.error:
                image = self.new()
                image.parent = self.uid
                image.kind = 'image'
                image.seq = req.seq or 0xFFFFFF  #place at end of siblings
                image.update(req)
                image.set_lineage()
                image.text = req.filename.replace('\\', '/').split(
                    '/'
                )[-1]  #store source filename for reference (fix MS brain-dead slashes, and strip off path)
                image.code = f"{image.uid}.{extension.lower()}"
                image.when = DATE()
                image.flush()  #store the image page
                image.renumber_siblings_by_kind()  #keep them in order
                # save the image file
                image.save_file(filedata, req.folder)
                # return
                req.message = f'image "{image.text}" added'
                return image
        return None

    def get_images(self):
        "sets self.images, i.e. child images (up to a maximum of 50 images), and returns it"
        if not hasattr(self, 'images'):
            if self.kind == "image":
                self.images = [self]
            else:
                self.images = self.list(
                    parent=self.uid,
                    kind="image",
                    orderby='seq,uid',
                    limit="50")
        return self.images

    def get_image(self):
        "return first image only, from get_images()"
        images = self.get_images()
        return images and images[0] or None

#    def delete_image(self):
#        "delete the image file and thumbnail and the image page"
#        self.delete_file()
#        try:  # this may not exist, so don't raise an error
#            os.remove(self.thumb_loc())  #delete the thumbnail
#        except:
#            pass

#    def delete_images(self):
#        "delete all child images"
#        for i in self.get_images():
#            i.delete() #delete the image page

    def remove_image(self, req):
        "delete image"
        self.delete() #delete the image page
        self.delete_tags()
        self.renumber_siblings_by_kind()
        return self.get_pob().redirect(req, "add_image")

#    remove_image.permit = 'edit page'
