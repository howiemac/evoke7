"""
config file for Page

"""

from evoke.data.schema import *

# the following are defaults and should not be removed - they are likely to be overridden in an app's config.py or config_site.py
show_who = True  # is author's name credited in page listings? (True or False) (note: It is always credited on page headers.)
show_when = True  # is the date/time of items shown on page listings? (True or False) (note: It is always shown on page headers.)
show_words = False # show the wordcount for each page (at the end of the page header)? (True or False)
show_rating = True # can pages be rated? If so they will be displayed on page headers and listings (True or False)
show_score = False # show the score on page headers and listings? (True or False)
order_by = "latest" # default page listing order 
thumb_size = 200 # image thumbnail size (width)  - this is storage size, they may be displayed smaller using CSS

# the following are overrides of config_base parameters
default_page = 1  # default "home" page to land on when no object/instance is specified
pagetabs = True

#the following data schemas may be subclassed or overidden in an app's config.py
class Page(Schema):
    table = 'pages'
    code = TAG, KEY
    parent = INT, KEY  # defines a hierarchy
    lineage = STR
    name = TAG, KEY
    kind = TAG, KEY
    stage = TAG, 'live', KEY
    when = DATE, now, KEY
    text = TEXT
#    text = TEXT, KEY  # if full-text search required...
    seq = INT, KEY
    rating = INT, KEY
    score = INT, KEY
    prefs = STR
    backlinks = STR
    # note that pages 1 and 2 have "live" stage so they aren't shown on child page listings and cannot be withdrawn
    insert = [
        dict(uid=1, parent=1, name='root', kind='page', lineage=".", stage='live'),
        dict(uid=2, parent=1, name='admin', kind='page', lineage=".1.", stage='live'),
    ]
