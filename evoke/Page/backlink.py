""" backlinks: a mix-in class for Page

Maintains the Page.backlinks columns of each pages linked to.

- NOTE: deals only with local database uid links in our format: [uid] or [uid caption]
- NOTE: extracts links from "posted" pages only

set_backlinks(): extracts links and creates a backlink in each unique page pointed to

    (calls delete_backlinks() first to delete all old backlinks to self)
    - should be called when a page is edited

get_backlinks(): returns a list of all backlinks of self

    - use this to display a list of backlinks for a page

delete_backlinks(): deletes all backlinks to self

    - should be called when a page is withdrawn

fix_backlinks(): browser utility to reset all backlinks for all pages (slow but dependable)

    - call this from the browser URL or an evo template (any page: All pages are always affected.)
    - use this for converting an existing pre-backlink database
    - use this for fixing any backlink data corruption eg due to bugs in other code

The following functions are primarily for internal use in this module:

    extract_links(): returns a list of all unique UIDs linked to in self.text

    store_backlinks(links): stores backlinks to self for a given sequence of page uids


Ian Howie Mackenzie (April 2020 / August 2021)
"""

import re
from evoke.lib import TEXT
from evoke.data import execute

class backlink(object):

    def get_backlinks(self):
        """ returns backlinks of self as a list (in reverse order)
        - deletes backlinks of self from database
        - req (request) is required if formatted==True
        - Note: doesn't find new backlinks - use fix_backlinks for this
        """
        backlinks= eval(self.backlinks) if self.backlinks else []
        backlinks.sort(reverse=True)
        return backlinks

    def extract_links(self):
        """ Extracts internal links from a posted page.
            Note: Only finds uid links in our format: [uid] or [uid caption]
        """
        # avoid draft pages etc
        if not (self.stage in ('posted','live')):
            return []
        # get the links (a list of all internal link uids from this page)
        rule = re.compile(r'(\[\ *)(\d+)')
        links = []
        for i in rule.findall(self.text):
            links.append(int(i[1]))
        # eliminate duplicates and return
        return list(set(links))

    def store_backlinks(self, links):
        """ Adds the links to the backlinks field of each target found.
        """
        for target in links:
            try:
                t = self.get(target)
                backlinks = eval(t.backlinks) if t.backlinks else []
                if self.uid not in backlinks:
                    backlinks.append(self.uid)
                    t.backlinks = str(backlinks)
#                print('backlinks now: ',t.backlinks)
                t.flush()
            except:
#                raise
                continue # ignore broken links
        return True

    def delete_backlinks(self):
        """ removes all backlinks to self
        """
        # remove all stored backlinks other than those at the beginning or end of a list
        execute(f'update {self.table} set backlinks=replace(backlinks," {self.uid},","")')
        # remove all stored backlinks at the beginning of a list
        execute(f'update {self.table} set backlinks=replace(backlinks,"[{self.uid}, ","[")')
        # remove all stored backlinks at the end of a list
        execute(f'update {self.table} set backlinks=replace(backlinks,", {self.uid}]","]")')
        # remove all stored backlinks in a list of 1 item
        execute(f'update {self.table} set backlinks=replace(backlinks,"[{self.uid}]","")')

    def set_backlinks(self):
        """ extract links and store them as backlinks
        """
        # remove existing backlinks pointing to this page
        self.delete_backlinks()
        # extract and store
        self.store_backlinks(self.extract_links())
        return True

    def fix_backlinks(self, req):
        """ Extract all backlinks for all posted pages.
            Recreate the backlinks fields from scratch.
        """
        # clear all backlinks fields
        execute(f"update {self.table} set backlinks = ''")
        # store all backlinks
        for p in self.list(isin={'stage':('posted','live')}):
            p.store_backlinks(p.extract_links())
        req.message='backlinks updated for all posted pages'
        return self.redirect(req)
#    fix_backlinks.permit="admin page"

