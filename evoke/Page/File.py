"""
Files - a Page kind

mix-in class for Page.py

- allows child pages of kind=="file" to be added which link to downloadable files
- also the basis for image files - see Image.py
- can be used as the basis for handling further file types (eg audio, video, whatever)

Notes:
- files are stored under their their original file name, in a data/<appname>/ subfolder
- when a file has the same name (page code) as another file already in the same subfolder, they both share the same file
- when a file page is removed (deleted), the corresponding file is left in place in its data/<appname> subfolder
    - this avoids complications with shared files
- use the pare_data utility routine to safely delete all files which are no longer referenced by any page

Ian Howie Mackenzie (2007, May 2019, March 2020, March 2022)
"""

from evoke.render import html
from evoke.lib import *

from io import StringIO
from os.path import lexists
import os
import shutil
from copy import copy

class File(object):
    ""

    filekinds = ["file"] # used by pare_data() below

    def file_url(self):
        "relative url"
        return f"/site/data/{self.file_folder()}/{self.code}"

    def edit_file(self, req):
        "file edit"
        if "name" in req:  #we have an update
            # store changes
            self.update(req)
            self.flush()
            return self.get_pob().redirect(req, '')
        return self.get_pob().redirect(req, 'add_file')

#    edit_file.permit = 'edit page'

    def add_file(self, req):
        ""
        _file = self.file_saved(req)
        if hasattr(self, 'files'):
            del self.files  # clear cache
        return self.file_add(req)

#    add_file.permit = 'edit page'

# pre 31 March 2022:
#    def file_loc(self, name=''):
#        "file location"
#        return '%sdata/%s/%s' % (self.Config.site_filepath,
#                                      self.file_folder(), name or self.code)

    def file_loc(self, name=''):
        "file location"
        return '%sdata/%s/%s' % (self.Config.site_filepath,
                                      self.file_folder(), self.code)

    def file_folder(self):
        """
        folder (within data folder) for a file - based on uid:
        - returns "123/456" for uid 123456789, so filepath will be 123/456/filename.ext
        - this allows us to exceed a billion files.... (or more precisely a billion pages)
        - if it over-rides, eg 1,234,567,890 we get 1234/567/filename.ext, so it doesn't immediately break....
        """
#        s = "%09d" % self.uid #pre 31 March 2022
        s = "%09d" % safeint(self.code.split(".")[0])
        return "%s/%s" % (s[:-6], s[-6:-3])

    def filedata(self, folder='', name=''):
        "return file data as bytes - generic, used also for images"
        d = '%sdata/%s' % (self.Config.site_filepath,
                                folder or self.file_folder())
        fp = '%s/%s' % (d, name or self.code)
        try:
            f = open(fp, 'rb')
            return f.read()
        except:
            return ''

    def save_file(self, content, folder="", name=''):
        "save actual file in filesystem - generic, used also for images"
        folders = folder or self.file_folder()
        datapath = '%sdata' % self.Config.site_filepath
        fp = '%s/%s/%s' % (datapath, folders, name or self.code)
        try:
            f = open(fp, 'wb')
        except:  # folder not found...
            d = '%s/%s' % (datapath, folders)
            os.makedirs(d)
            #      os.makedirs(d,02777) # doesn't work reliably
            #     so, fix permits
            d = datapath
            for folder in folders.split('/'):
                d = "%s/%s" % (d, folder)  #append folder
                os.chmod(d, 0o2777)
            f = open(fp, 'wb')
        os.chmod(fp, 0o2664)
        f.write(content)

    def file_saved(self, req):
        """
        if req is valid, stores new file and returns Page object, else returns 0.
        - REQUIRES req.filedata and req.filename
        - Folder can optionally be specified in req.folder
        """
        if 'filedata' in req:
            filedata = req.filedata
            extension = req.filename.split(".")[-1].lower()
            name = req.filename.replace('\\', '/').split('/')[
                -1]  # fix MS brain-dead slashes, and strip off path
            #      print ">>>>>>>>>>>>>>>>>>>>",req.filename,extension
            if not filedata:
                req.error = "please provide a file"
            if not req.error:
                _file = self.new()
                _file.parent = self.uid
                _file.kind = 'file'
                _file.seq = req.seq or 0xFFFFFF  # place at end of siblings
                _file.name = name
                _file.stage = 'live'
                _file.update(req)
                _file.set_lineage()
                _file.code = f"{_file.uid}.{extension.lower()}"
                _file.when = DATE()
                # store file page
                _file.flush()
                _file.renumber_siblings_by_kind()  #keep them in order
                # save the file
                _file.save_file(filedata, req.folder)
                # return
                req.message = f'file "{_file.name}" added'
                return _file
        return None

    def get_files(self):
        "set self.files, i.e. a list of all child file objecs, if it is not already set, and returns it"
        if not hasattr(self, 'files'):
            self.files = self.list(
                parent=self.uid, kind="file", orderby='seq,uid')
        return self.files

#    def delete_file(self):
#        "delete the file and the file page"
#        self.delete()  #delete the file page
#        try:
#            os.remove(self.file_loc())  #delete the file
#        except:
#            pass  # delete failed and we don't care :)

#    def delete_files(self):
#        "delete all child files"
#        for i in self.get_files():
#            i.delete() #delete the file page

    def remove_file(self, req):
        "delete file"
        self.delete()  #delete the file page
        self.delete_tags()
        self.renumber_siblings_by_kind()
        return self.get_pob().redirect(req, 'add_file')

#    remove_file.permit = 'edit page'

    @classmethod
    def pare_data(self,req):
        """prunes all obsolete files from the data folder
        WARNING - ANY BUGS IN THIS CAN DESTROY YOUR DATA - MAKE A BACKUP FIRST!
        Optional request parameter: ?source=<your data folder>
        1) moves all data to an "x_<appname>" folder
        2) then moves valid data back to source
        3) thus obsolete data remains in the "x<appname>" folder, for manual deletion
        Assumes a proper filesystem (ie not fat or exfat!)
        """
        # data folder
        appname=self.Config.appname
        source=req.source or os.path.realpath(self.Config.site_filepath+"data")+'/'
        info=f"paring data in {source} <br/><br/>"
        # rename the source folder, and create dest folder (as the original source)
#        print(f'moving "{source}" folder to "x_{source}"')
        dest=copy(source)
        source=f"/x_{appname}/".join(source.rsplit(f"/{appname}/", 1))
        os.rename(dest,source)
        # move the valid files to the original folder
#        print("moving back the valid files...")
        c=0
        for i in self.list(isin={'kind':self.filekinds},orderby="uid"):
            c+=1
            fn=f"{i.file_folder()}/{i.code}"
#            print("keeping ",fn)
            try:
                os.renames(source+fn,dest+fn)
            except:
                pass # exception thrown for every file where code does not match uid
        info+=f"{c} files retained  <br/><br/>"
        # count the deletions
        d = 0
        for root_dir, cur_dir, files in os.walk(source):
            d += len(files)
        info+=(f"{d} files deleted<br/>")
#        print("done: " ,d ,"files deleted;",c ," files retained")
        # try to delete the x_ folder (tree)
        try:
            shutil.rmtree(source)
        except:
            pass
        # return results
        return info


    def fix_filecodes(self,req):
        "fix all file codes from pre 17/3/2022 format to new format (same as image)"
        info="fixing all invalid file codes<br/>"
        for f in self.list(kind="file"):
            name,ext = f.code.rsplit(".",1)
            if safeint(name)!=f.uid: # avoid valid new form codes
                f.text = f.code
                f.code = f"{f.uid}.{ext.lower()}"
                info+=f"file uid {f.uid} code:{f.text} => {f.code}<br/>"
                f.flush()
            # fix filenames
            oldname= f.file_loc(name=f.text)
            newname= f.file_loc()
            try:
              os.rename(oldname,newname)
              info+=f"file {oldname} => {newname}</br>"
            except OSError as e:
              info+=f"ERROR: {oldname} => {newname} : {e.strerror}<br/>"
        return info

