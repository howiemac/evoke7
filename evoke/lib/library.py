"""
evoke library routines

a library of robust general routines mainly for handling conversions

weekdayname                  array of weekday names
monthname                    array of month names
def http_date(future=0):     returns HTTP format date-time string: future is seconds from now, and result is like "Sun, 29 Mar 2020 13:50:06 GMT"
def elapsed(seconds)         converts time in seconds into hours:mins:secs
def process_time():          returns time elapsed since system started, in a display format
def turnaround(start,end):   calculates turnaround, in working days, ie ignoring weekends, accepts dates in any format

def asItems(text):
def counted(items,start=1):  converts list of items into list of (count,item) pairs
def safeint(num):            converts to int, regardless
def sn(ok):                  converts boolean value to +1 or -1

def limit(v,lo,hi):          limits v to between lo and hi

def percent(a,b):            returns string repr of a as a percentage of b (to nearest full percent)

def prev(p, seq):            returns previous object from seq, starting at p
def next(p, seq):            returns next object from seq, starting at p

def page(req,pagesize=50):   for paging SQL results: returns limit parameter for data.data.list()

def delf(text):              remove carriage-returns and single line-feeds from text
def replace(str,old,new):    case insensitive string replacement

def idf(t):                  fixes t into an id which wont break html - not foolproof

def url_safe(text):          fixes url for http

def csv_format(s):           cleans syntax problems for csv output

def sql_list(val):           converts iterable into sql format list for 'is in' etc.

def safe_filename(text):     filename formatter - not foolproof


(Ian Howie Mackenzie 2/11/2005 onwards. Email enhancements by Chris J Hurst)
"""

from time import time, gmtime
import datetime
from .DATE import DATE
import urllib.request, urllib.parse, urllib.error, re

###################### time ###################


weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

monthname = [None,
              'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

def http_date(future=0):
    """
    returns HTTP format date-time string, like: "Sun, 29 Mar 2020 13:50:06 GMT"
    - future is number of seconds from now
    (adapted from http.cookies by Timothy O'Malley)
    """
    now = time()
    year, month, day, hh, mm, ss, wd, y, z = gmtime(now + future)
    return "%s, %02d %3s %4d %02d:%02d:%02d GMT" % \
           (weekdayname[wd], day, monthname[month], year, hh, mm, ss)


def elapsed(seconds, format=""):
    """converts time in seconds into days hours mins secs
    format, if given, can be "d:h:m:s", or "h:m:s", or "m:s", otherwise long format is used
    ( adapted from zope ApplicationManager.py )
    """
    s = safeint(seconds)
    d = 0
    h = 0
    if (not format) or format.startswith("d:"):
        d = int(s / 86400)
        s = s - (d * 86400)
    if (not format) or ("h:" in format):
        h = int(s / 3600)
        s = s - (h * 3600)
    m = int(s / 60)
    s = s - (m * 60)
    if format:
        if d:
            return ('%d:%02d:%02d:%02d' % (d, h, m, s))
        if h:
            return ('%d:%02d:%02d' % (h, m, s))
        return ('%d:%02d' % (m, s))
    else:  # long format
        d = d and ('%d day%s' % (d, (d != 1 and 's' or ''))) or ''
        h = h and ('%d hour%s' % (h, (h != 1 and 's' or ''))) or ''
        m = m and ('%d min' % m) or ''
        s = '%d sec' % s
        return ('%s %s %s %s' % (d, h, m, s)).strip()


def process_time():
    s = int(time()) - process_start
    return elapsed(s)

process_start = int(time())


def turnaround(start, end):
    """calculates turnaround, in working days, ie ignoring weekends
       accepts dates in any format
    """
    start = DATE(start).datetime.date()
    end = DATE(end).datetime.date()
    t = end - start
    s = start.weekday()
    e = end.weekday()
    d = (7 - s) + e
    return int((((t.days - d) * 5) // 7) + d - 1)


###################### number conversion ###################


def asItems(text):
    try:
        n = int(text)
        if n == 0:
            return ''
        return repr(n)
    except:
        return ''


def counted(items, start=1):
    "converts list of items into list of (count,item) pairs"
    n = start
    z = []
    for i in items:
        z.append((n, i))
        n += 1
    return z


def safeint(num):
    """converts to int, regardless
  """
    try:
        v = int(num)
    except:
        v = 0
    return v


def safefloat(num):
    """converts to float, regardless
  """
    try:
        v = float(num)
    except:
        v = 0.0
    return v


def sn(ok):
    """converts boolean value to +1 or -1
  """
    if ok: return 1
    else: return -1


# number utilities ##################


def limit(v, lo, hi):
    "limits v to between lo and hi"
    return min(hi, max(lo, v))


# number formatting ############


def percent(a, b):
    "returns string repr of a as a percentage of b (to nearest full percent)"
    return '%d%%' % (0.5 + float(a) * 100 / float(b))


################# list utilities ###############


def prev(p, seq):
    "expects current object 'p', and sequence of objects 'seq' - returns previous object"
    try:
        i = seq.index(p)
        if i:
            return seq[i - 1]
        else:
            return None
    except:
        return None


def next(p, seq):
    "expects current object 'p', and sequence of objects 'seq' - returns next object"
    try:
        return seq[seq.index(p) + 1]
    except:
        return None


################### paging SQL results ####################


def page(req, pagesize=50):
    """ enables easy paging of SQL results via SQL limit clause
       - returns SQL limit parameters for data.data.list():
       - also sets req.pagesize, req.pagenext, and req.SQLlimit 
       - if no req.pagenext given, it defaults to 0
       - *requires req.pagenext for second and subsequent pages*
       - increments req.pagesnext by req.pagesize
       - sets req.pagesize to pagesize (int)
       - sets req.SQLlimit to SQL limit parameters (str)
    """
    offset = safeint(req.pagenext)
    req.pagenext = offset + pagesize  #next page
    req.pagesize = pagesize  #for use in form, to determine whether there is more to show
    req.SQLlimit =  f"{offset}, {pagesize}"
    return req.SQLlimit


################### text formatting ##################

def delf(text):
    """remove carriage-returns and single line-feeds from text
       - leaves double line-feeds (paragraph breaks)
       - also removes trailing spaces
       - source must be a string
    """
    # remove trailing spaces
    lines=[line.rstrip() for line in text.split('\n')]
    cleantext='\n'.join(lines)
    # remove carriage returns and single line feeds
    return cleantext.replace('\r','').replace('\n\n','\r').replace('\n',' ').replace('\r','\n\n')

def unwrap(text):
    """adds 2 spaces to the end of each line in text
       - prevents markdown from wrapping lines
       - used for poetry, etc
    """
    # add trailing spaces
    return text.replace('\n','  \n')

def wrap(text):
    """removes spaces from the end of each line in text
       - so that markdown will wrap the lines
       - undoes unwrap()
    """
    # remove trailing spaces
    lines=[line.rstrip() for line in text.split('\n')]
    return '\n'.join(lines)

def replace(str,old,new):
    "case insensitive string replacement"
    return re.sub(re.escape(old), new, str, flags=re.IGNORECASE)

################### html formatting ###################


def idf(t):
    "fixes t into an id which wont break html - not foolproof"
    return t.replace(' ', '').replace('(', '').replace(')', '')


################### http formatting ###################

#def url_safe(text):
#  return urllib.quote(text)


def url_safe(text):
    return urllib.parse.quote_plus(text, safe="/")


################### CSV formatting ###################
# O/S - THIS SHOULD BE SOMEWHERE ELSE.... a reporting library?


def csv_format(s):
    """
    cleans syntax problems for csv output
  """
    s = s.replace('"', "'")
    s = s.replace("\n", " ")
    s = s.replace("\x00D", "")
    return '"%s"' % s


################# SQL formatting ###################


def sql_list(val):
    "converts iterable into sql format list for 'in' etc."
    if not val:
      return '(NULL)'
    return len(val) == 1 and f'({repr(tuple(val)[0])})' or tuple(val)


################ filename formatting ###############


def safe_filename(text):
    "not foolproof"
    return text.replace(' ', '_').replace("'", "").replace('"', '').replace(
        '/', '-').replace('?', '').replace('!', '')


#def test():
#    pass
#
#if __name__ == '__main__':
#    test()
