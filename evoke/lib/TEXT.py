"""
This module implements the TEXT class (a.k.a. TEXT)

TEXT is used for transient storage and manipulation of strings in markdown format.

O/S Currently assumes that Page.py is in use....

Implements self.formatted() and self.summarised(), which format text to HTML,
using markdown syntax.

see https://python-markdown.github.io/

Standard markdown links are enhanced by our own [uid name] link syntax:
- this allows uid numbers to be used in place of a url
- e.g. [123 some page]
- this works like: [some page](/page/123)
- the markdown link syntax still works also.

Note: markdown is loaded with the "extra" extension, allowing:

- Abbreviations
- Attribute Lists
- Definition Lists
- Fenced Code Blocks
- Footnotes
- Tables
- Markdown in HTML

see https://python-markdown.github.io/extensions/extra/

"""

import re

from markdown import Markdown
#from html2text import html2text

from .evolinks import EvoLinkExtension
from .STR import STR
from .INT import INT
from .library import safeint

def markdown(text, req, *a, **k):
    "markdown with extra extension"
    extensions = k.setdefault('extensions', ["extra"])
    extensions.append(EvoLinkExtension())
    md = Markdown(*a, **k)
    md.req = req
    return md.convert(text)

class TEXT(STR):
    """
  Evoke text format handling
  """

    # extra rules for markdown
    image_rule = re.compile(r'(\[IMG )(.*?)(\])')

    def summarised(self, req, chars=250, lines=3, formatted=True):
        " return summary of formatted text "
        if formatted:
            return self.formatted(req, chars, lines)
        # not formatted - ignore "lines"
        return self[:chars]

    def formatted(self, req, chars=0, lines=0):
        "format in HTML via Markdown"
        self.has_more = False
        # get the text to process
        text = self
        if chars:
            self.has_more = chars < len(self)
            text = self[:chars]
        if lines:
            z = text.splitlines()
            textlines = z[:lines]
            text = "\n".join(textlines) + '\n'
            self.has_more = self.has_more or (lines < len(z))

        def subimage(match):
            "render a [IMG (uid|url) attributes?]"
            source = match.groups()[1]
            # print (match.groups())
            if ' ' in source:
                url, atts = source.split(' ', 1)
            else:
                url, atts = source, ''

            # check for a valid uid
            if safeint(url):
                try:
                    img = req.user.Page.get(safeint(url))
                    url = img.thumb_url()
                except:
                    raise
                    pass

            return '<img src="%s" %s />' % (url, atts)

        text = self.image_rule.sub(subimage, text)
        return markdown(text, req)

