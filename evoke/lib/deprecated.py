"""
deprecated library routines  (retained for backwards compatibility only - do not use otherwise)
===============================================================================================

def httpDate(when=None, rfc='1123'): use http_date() - see library.py


class deepdate:   	     use DATE


def hours(text,negate=0):    converts input string, or number of minutes, to display format ie 'hh:mm'
			     returns '' rather than '0:00'
			     allows for text fractions eg '1.5' giving '1:30'
			     note: value() converts the opposite way
def value(text):             converts 'money' format string to long integer in pence, 
			     converts 'hours' format string to minutes long integer
			     returns 0 for empty or invalid strings
			     if it gets a number, returns that as long integer
def money(text,negate=0,zeros=0):  converts input string, or number of pence, to user 'money' text format ie '?.??'

def redirect(req,url): does a response redirect, ie http status code 302 - use as: return redirect(req,url)

greenstripe="#C8F8E8"
greystripe="#AAAAAA"
red="red"
yellow="yellow"
orange="#FF6600"
green="green"
cyan="#0066CC" #"cyan"
blue="blue"
black="black"

(IHM April 2007)


"""

from .DATE import DATE as deepdate
from time import time,gmtime
import re


###################### time ###################

def httpDate(when=None, rfc='1123'):
    """ (copied as is from MoinMoin request.py)
    Returns http date string, according to rfc2068

    See http://www.cse.ohio-state.edu/cgi-bin/rfc/rfc2068.html#sec-3.3

    A http 1.1 server should use only rfc1123 date, but cookie's
    "expires" field should use the older obsolete rfc850 date.

    Note: we can not use strftime() because that honors the locale
    and rfc2822 requires english day and month names.

    We can not use email.Utils.formatdate because it formats the
    zone as '-0000' instead of 'GMT', and creates only rfc1123
    dates. This is a modified version of email.Utils.formatdate
    from Python 2.4.

    @param when: seconds from epoch, as returned by time.time()
    @param rfc: conform to rfc ('1123' or '850')
    @rtype: string
    @return: http date conforming to rfc1123 or rfc850
    """
    if when is None:
        when = time()
    now = gmtime(when)
    month = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct',
        'Nov', 'Dec'
    ][now.tm_mon - 1]
    if rfc == '1123':
        day = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][now.tm_wday]
        date = '%02d %s %04d' % (now.tm_mday, month, now.tm_year)
    elif rfc == '850':
        day = [
            "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
            "Sunday"
        ][now.tm_wday]
        date = '%02d-%s-%s' % (now.tm_mday, month, str(now.tm_year)[-2:])
    else:
        raise ValueError("Invalid rfc value: %s" % rfc)
    return '%s, %s %02d:%02d:%02d GMT' % (day, date, now.tm_hour, now.tm_min,
                                          now.tm_sec)





def _hours(text, rex=re.compile('(-?)([0-9]*)\:?([0-5]?[0-9]?)')):
    """ used by hours() and value()
  """
    return rex.match(text).groups()


def hours(text, negate=0):
    """ converts input string, or number of minutes, to display format ie 'hh:mm'
      returns '' rather than '0:00'
      allows for text fractions eg '1.5' giving '1:30'
      note: value() converts the opposite way
  """
    try:  # catch decimal fractions entered as text
        text = float(text)
    except:
        pass
    if not isinstance(text,
                      type('')):  #if not a string, assume we have a number
        try:
            text = "%d:%s" % divmod(int(text * 60), 60)
        except:
            return ''
    sign, hours, minutes = _hours(text)
    if negate:  #toggle sign
        sign = sign and ' ' or '-'
    text = "%s%s:%s" % (sign, hours or '0', minutes.zfill(2))
    return text != "0:00" and text or ""


####################### money ##############


def value(text):
    """converts 'money' format string to long integer in pence, 
     converts 'hours' format string to minutes long integer
     returns 0 for empty or invalid strings
     if it gets a number, returns that as long integer
  """
    if not isinstance(
            text, type('')):  #if not a string, perhaps it is already a number?
        return safeint(text)
    try:
        sign, pounds, pence = _money(text.strip())
        v = (safeint(pounds) * 100) + safeint(pence)
        return sign and -v or v
    except:
        try:  #have we 'hours' format?
            sign, hours, minutes = _hours(text)
            return (safeint(hours) * 60) + safeint(minutes)
        except:
            return 0


def _money(text, rex=re.compile('(-?)([0-9]*)\.?([0-9]*)')):
    """ used by money() and value()
  """
    return rex.match(text).groups()


def money(text, negate=0, zeros=0):
    """ converts input string, or number of pence, to user 'money' text format ie '?.??'
  """
    if not isinstance(text,
                      type('')):  #if not a string, assume we have a number
        try:
            text = str(text)  #convert to string
            if (len(text) == 1):
                text = '0' + text
            elif ((len(text) == 2) and (text[0] == '-')):
                text = '-0' + text[1]
            text = "%s.%s" % (text[:-2], text[-2:])  # add decimal point
        except:
            return ''
    text = text.replace(',', '')  #remove all commas
    sign, pounds, pence = _money(text)
    if pounds == '' and pence == '00':
        return ''
    if negate:
        sign = (not sign) and '-' or ''
    pence = pence and (pence + '0')[:2] or '00'
    return "%s%s.%s" % (sign, pounds or '0', pence)


################### html formattingr ###################

# colour constants:
greenstripe = "#C8F8E8"
greystripe = "#AAAAAA"
red = "red"
yellow = "yellow"
orange = "#FF6600"
green = "green"
cyan = "#0066CC"  #"cyan"
blue = "blue"
black = "black"
