""" evoke html generation:

a decorator to create simple calling functions for Evo (See evo.py)

"""

from re import compile
from evoke.render.evo import Evo, EvoTemplateNotFound
import gettext
import os

class Html(object):
    "a decorator to create an evocative function"

    def __init__(self, *a, **k):
        ""
        self.a = a
        self.k = k

    def __call__(self, fn):
        "find the correct name of the template for this function then call it.  Allow for classmethods and kind override"
        fname = fn.__name__

        # we don't know much about this method at class creation time eg. it won't become a method
        # or classmethod until after this decorator is called.
        self.template_cache = {}

        def function(inner_self, req, *a, **k):
            "a typical template"

            # we know more about the method when it is called.
            # is this a classmethod?
            if isinstance(inner_self, type):
                klass = inner_self
            else:
                klass = type(inner_self)

            # never mind the klass hierarchy for now, just assume the primary one..
            klass_name = klass.__bases__[0].__name__

            # set language
            lang = self.getLang(inner_self, req)
            req.gettext = lang
            # run the function, and return the template
            fn(inner_self, req)

            # try the template
            template_name = f"{klass_name}_{fname}.evo"
            if template_name in self.template_cache:
                template = self.template_cache[template_name]
            else:
                template = Evo(template_name) # parse the EVO code
                self.template_cache[template_name] = template
            try:
                return template(inner_self, req, gettext=lang)
            except:
                raise
        return function

    def getLang(self, inner_self, req):
        "return gettext language object"
        code = req.cookies.get('lang', '')
        #    print "getLang", code
        if code:
            try:
                trans_domain = inner_self.Config.appname
                trans_path = os.path.join(inner_self.Config.app_fullpath,
                                          'trans')
                lang = gettext.translation(
                    trans_domain, trans_path, languages=[code])

#        print "lang found", lang, type(lang)
            except:
                # fallback on plain, no-op gettext
                #        print "lang not found"
                lang = gettext
            return lang.gettext
        else:
            #      print "no language specified"
            return gettext.gettext

html = Html()
