# -*- coding: utf-8 -*-
"""
config file for evoke.Permit
"""

from evoke.data.schema import *

# the following are likely to be overridden in an app's User/config.py
guests = False  # do guests have access by default?
permits = {
    'master': ['be'],
    'user': ['edit'],
    'page': ['create', 'edit', 'admin']
}  #basic permits, plus Page klass permits
default_permits = {
    'page': ['create', 'edit']
}  #default permits a new user gets


class Permit(Schema):
    table = 'permits'
    user = REL, KEY
    group = TAG
    task = TAG, 'view'
    insert = [dict(user=2, group='master', task='be')]
