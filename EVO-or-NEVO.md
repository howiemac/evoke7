# 

by Ian Howie Mackenzie (April 2021)

I intend to replace Evo with Nevo for Evoke 7.

Currently Evo remains in use because of issues with Nevo, which will hopefully be addressed soon.


### Evo-related code issue

There was a loop section in render/html.py to check for alternate evo filename conventions.

I have removed this (from my current master branches of Evoke 6 and 7) as its implementation badly broke Evo's error reporting.

This was connected (I think) to the first section of code in Page.py, headed:

    # #### Kinds - convenience methods to ease listing

I don't use this code at all (and it's removed from Grace)

- It came from Chris Hurst, and enhanced earlier code which allowed for image and file kinds to be klasses (not required now).
- I think it's related to the code I removed from html.py...

My problem with this code is simply that, as I don't use it, I am unclear on how to test it.